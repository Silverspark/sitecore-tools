var protocol = "https://";
var domain = "www.cerner.com";
var preUrl = protocol + domain;

function updateDomain() {
    domain = $('#selectDomain').val();
    preUrl = protocol + domain;
}

$('#convertGuid').click(function () {
    updateDomain();
    var guid = $('#guid').val();
    if (guid != "") {
        guid = guid.replace(new RegExp('-', 'g'), "");
        guid = guid.replace('{', "");
        guid = guid.replace('}', "");
        mediaLink = preUrl + '/-/media/' + guid + '.ashx';
        pageLink = preUrl + '/~/link.aspx?_id=' + guid + '&amp;_z=z';
        ceMediaLink = "~/media/" + guid + '.ashx';
        cePageLink = '~/link.aspx?_id=' + guid + '&amp;_z=z';
        $('#mediaLink').val(mediaLink);
        $('#pageLink').val(pageLink);
        $('#ceMediaLink').val(ceMediaLink);
        $('#cePageLink').val(cePageLink);
    } else {
        $('#mediaLink').val("");
        $('#ceMediaLink').val("");
        $('#pageLink').val("");
        $('#cePageLink').val("");
    }
});

$('#convertPath').click(function () {
    updateDomain();
    var path = $('#sitecorePath').val();
    if (path != "") {
        if (path.indexOf('/sitecore/content') != -1) {
            path = path.replace(/^.*?\/Home/i, "");
        }
        if (path.indexOf('/sitecore/media library') != -1) {
            path = path.replace('/sitecore/media library/','/~/media/')
        }
        if (path.indexOf('_content') != -1) {
            path = path.replace(/\/_content.*/i, "");
        }

        if ($('#convertSpaces').prop('checked')) {
            path = path.replaceAll(' ', '-');
        }

        var pageUrl = preUrl + path;
        $('#pageUrl').val(encodeURI(pageUrl));
    } else {
        $('#pageUrl').val("");
    }
});