# Sitecore Tools

This is a simple program to help convert Sitecore item information into a useful format for various purposes. It's a quick and dirty bit of JS code on a boring page now. 
I may pretty it up and use it as a simple concept to test out a new technology and continue my own personal education. 

## Installation

Just drop it in a publically accessible web directory or on your system and go, all self contained in one html and one js file for now (leverages jQuery and Bootstrap via CDN).